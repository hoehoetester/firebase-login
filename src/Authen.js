import React, { Component } from 'react';
import firebaseSettings from './firebase.settings';
var firebase = require('firebase');

const config = firebaseSettings;

firebase.initializeApp(config);

class Authen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignedIn: false
        };
        this.signin = this.signin.bind(this);
        this.signup = this.signup.bind(this);
        this.signout = this.signout.bind(this);
        this.google = this.google.bind(this);
    }

    signin(e) {
        const email = this.refs.email.value;
        const password = this.refs.password.value;
        console.log(email, password);

        const auth = firebase.auth();

        const promise = auth.signInWithEmailAndPassword(email, password);

        promise
            .then(user => {
                this.setState({
                    isSignedIn: true,
                    msg: `welcome ${user.email}`
                });
            })
            .catch(e => {
                let err = e.message;
                console.log(err);
                this.setState({
                    msg: err
                });
            });
    }

    signup() {
        const email = this.refs.email.value;
        const password = this.refs.password.value;
        console.log(email, password);
        const auth = firebase.auth();
        const promise = auth.createUserWithEmailAndPassword(email, password);

        promise
            .then(user => {
                let msg = `welcome ${user.email}`;
                firebase.database().ref('users/' + user.uid).set({
                    email: user.email
                });
                console.log(1, user);
                this.setState({ msg });
            })
            .catch(e => {
                let err = e.message;
                console.log(err);
                this.setState({ msg: err });
            });
    }

    google() {
        let provider = new firebase.auth.GoogleAuthProvider();
        let promise = firebase.auth().signInWithPopup(provider);

        promise
            .then(result => {
                let user = result.user;
                console.log(user);
                firebase.database().ref('users/' + user.uid).set({
                    email: user.email,
                    name: user.displayName
                });
            })
            .catch(e => {
                let msg = e.message;
                console.log(msg);
                this.setState({ msg });
            });
    }

    signout() {
        firebase.auth().signOut();
        this.setState({ isSignedIn: false });
        this.setState({ msg: 'good-bye, login again' });
    }

    render() {
        let _isSignedIn = this.state.isSignedIn;
        return (
            <div>
                <input type="email" id="email" ref="email" placeholder="email" defaultValue="aaa@aaa.aaa" className={_isSignedIn ? 'hidden' : ''} /><br />
                <input type="password" id="password" ref="password" placeholder="password" defaultValue="aaaaaa" className={_isSignedIn ? 'hidden' : ''} /><br />
                <button onClick={this.signin} className={_isSignedIn ? 'hidden' : ''}>sign in</button>
                <button onClick={this.signup} className={_isSignedIn ? 'hidden' : ''}>sign up</button>
                <button id="signout" onClick={this.signout} className={_isSignedIn ? '' : 'hidden'} >sign out</button>
                <button id="google" onClick={this.google} className={_isSignedIn ? 'hidden' : ''}>google</button>
                <p>{this.state.msg}</p>
            </div>
        );
    }
}

export default Authen;